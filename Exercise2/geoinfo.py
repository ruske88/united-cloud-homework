"""
Small script to get geolocation info and store to sqlite
"""
import datetime
import time
import sqlite3
import collections
import requests

ALL_KEYS = dict()
TRACKING_KEYS = list()
DB = sqlite3.connect("db/info.db")
C = DB.cursor()
API_KEY = "75ec9f3d9bef3c30d8f86c636ab599f7"
URL = "http://api.ipstack.com/check"
ACCESS = {'access_key': API_KEY}
R = requests.get(URL, params=ACCESS)

CREATE_TABLE = "create table if not exists geoinfo(timestamp text primary key, %s)" \
               % ", ".join(TRACKING_KEYS)


def find_all_keys(dic, prefix=""):
    """Function to discover all keys in json response"""
    for key, value in dic.iteritems():
        if isinstance(value, dict):
            prefix = prefix + key + "__"
            find_all_keys(value, prefix)
        elif isinstance(value, list):
            count = 0
            for i in value:
                prefix2 = prefix + key + "__" + str(count) + "__"
                find_all_keys(i, prefix2)
            continue
        else:
            ALL_KEYS[prefix + key] = value
    return collections.OrderedDict(sorted(ALL_KEYS.items()))


def init_db():
    """Initialize DB based on discovered json keys in response"""
    info = find_all_keys(R.json())
    for key in info.keys():
        TRACKING_KEYS.append(key)
    C.execute("create table if not exists geoinfo(timestamp text primary key, %s)"
              % ", ".join(TRACKING_KEYS))


def number_of_values(values):
    """Function to dinamically prepare insert number of fields we want to insert in DB"""
    return '({})'.format(', '.join('?' * len(values)))


init_db()

while True:
    R = requests.get(URL, params=ACCESS)
    MY_INFO = find_all_keys(R.json())
    TIMESTAMP = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    VALUES = list()
    VALUES.append(TIMESTAMP)
    for item in TRACKING_KEYS:
        VALUES.append(MY_INFO[item])
    NUM = number_of_values(VALUES)
    C.execute("insert into geoinfo values {}".format(NUM), VALUES)
    DB.commit()
    print "Data inserted in db"
    time.sleep(60)
