Provisioning:
	Ansible:
		No need for running agents on every host, everything can be done using ssh keys.
		Creating playbooks using YAML format, it had good readability and usability
	Puppet:
		Need dedicated master and db server and have to run puppet agent on every host, but in difference to Ansible, you cannot perform manual things on host because agent will override it in every run
		In case of syntax error, process won't start at all, while with Ansible you can perform few steps in execution before hitting syntax error
		Downside is bad readability as it uses own declarative language

Monitoring:
	Nagios:
		Good for host, service and network monitoring.
		Extendable by custom monitoring script.
	New Relic:
		Good for application monitoring as you can run agent within your application and get a lot of different app metrics.
		Phone application available.

Backup:
	No specific tool. Most important thing is to have incremental and full backup and to be able to restore easily.
	Good option would be to be able to backup full OS, but if that takes to many disk space,
		it can be done by backing up only important app specific data, and restore process will be to re-provision machine
		from beginning and restore backed up app data.

Hypervisor:
	VMware vSphere:
		It has broad OS support. No OS necessary to control virtualization components.

Logging:
	Splunk:
		Easy configurable file list to collect logs which can be visible in UI by installing splunk forwarder on host
		Very intuitive search with lot of options
		Downside is that is pretty expensive
	ELK:
		Open source and therefore better community support.
		Kibana dashboard provide more personalization then Splunk.


